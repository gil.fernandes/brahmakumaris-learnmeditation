const axios = require('axios').default;
const html_to_pdf = require('html-pdf-node');

const generatePdf = async (content) => {
  const options = { format: 'A4' };
  let file = { content };
  return await html_to_pdf.generatePdf(file, options)
}

module.exports = {
  // GET /pdf/module
  pdfModule: async ctx => {
    strapi.log.warn('PDF Export query parameters.', ctx.query);
    const id = ctx.query.id ? parseInt(ctx.query.id) : 2
    const response = await axios.get(`https://admin.learnmeditationonline.org/modules/${id}`)
    console.log('response', response)
    const data = response.data
    const pageName = data.page.pageName
    const pageContent = data.page.pageContent
    const content = `<h1>${pageName}</h1><div>${pageContent}</div>`
    const pdfBuffer = await generatePdf(content)
    strapi.log.warn('Generated PDF buffer');
    ctx.attachment(`module_${id}_section_${data.section.id}.pdf`);
    ctx.type = 'application/pdf';
    ctx.body = pdfBuffer
  }
};
