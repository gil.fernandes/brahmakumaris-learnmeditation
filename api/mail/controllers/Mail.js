const sgMail = require('@sendgrid/mail')
const { parseMultipartData } = require('strapi-utils')

const styleQuestion = 'style="border: 1px solid #aaaaaa; border-radius: 10px; padding: 5px 20px; font-weight: bold; font-family: Arial, Helvetica, sans-serif;"'
const styleAnswer = 'style="padding: 10px 20px 40px 20px"'

const contactMessageLimit = 1024

const englishContactEmails = ['gil.fernandes@gmail.com', 'anne.waltham@gmail.com']

const allContactEmails = {
  "en": englishContactEmails,
  "de": ['gil.fernandes@gmail.com', "carolin.fraude@de.brahmakumaris.org"],
  "fr": ['gil.fernandes@gmail.com', "marie.edery@free.fr", "paris@rajayogameditation.fr"]
}

const langTokens = {
  "en": {
    "Contact request from": "Contact request from",
    "Original message from": "Original message from"
  },
  "de": {
    "Contact request from": "Kontaktanfrage von",
    "Original message from": "Nachricht from"
  },
  "fr": {
    "Contact request from": "Demande de contact de",
    "Original message from": "Message original de"
  }
}

const chooseTargetEmails = (language) => {
  if(!language || !allContactEmails[language]) {
    language = "en"
  }
  return allContactEmails[language]
}

const getLangToken = (language, key) => {
  if(langTokens[language] && langTokens[language][key]) {
    return langTokens[language][key]
  }
  return key
}

const processMultipartContent = data => {
  const questionList = Object.entries(data).
    filter(kv => kv[0].match(/^(?!email).+/)).
    filter(kv => kv[0].match(/^(?!title).+/)).
    filter(kv => kv[0].match(/^(?!securityCode).+/)).
    map(
      kv => `<dt ${styleQuestion}>${kv[0]}</dt><dd ${styleAnswer}>${kv[1]}</dd>`)
  return `<dl>${questionList.join('')}</dl>`
}

const extractSingleItem = (data, regExp) => {
  const items = Object.entries(data).filter(kv => kv[0].match(regExp))
  return items.length > 0 ? items[0][1] : null
}

const extractEmail = data => extractSingleItem(data, /^email.*/)

const extractTitle = data => extractSingleItem(data, /^title.*/)

const extractSecurityCode = data => extractSingleItem(data, /^securityCode.*/)

const securityCodeOk = (code) => code === '11191234123123123131312312213231' // Dummy implementation of security code checking.
const securityContactCodeOk = (code) => code ===
  '12291234123123123131312312213231' // Dummy implementation of security code checking.

function extractParameters (ctx) {
  let mailContent = '<strong>Welcome to learnmeditationonline!</strong>'
  let targetEmail = 'gil.fernandes@gmail.com'
  let subject = 'Unknown subject'
  let securityCode = null
  if (ctx.is('multipart')) {
    const { data } = parseMultipartData(ctx)
    mailContent = processMultipartContent(data)
    targetEmail = extractEmail(data)
    subject = extractTitle(data)
    securityCode = extractSecurityCode(data)
  }
  return { mailContent, targetEmail, subject, securityCode }
}

const buildMessage = (targetEmail, subject, mailContent) => ({
  to: targetEmail,
  from: 'gil.fernandes@bkconnect.net',
  subject: subject,
  text: mailContent.replace(/(<([^>]+)>)/ig, ''),
  html: mailContent,
})

const setSendGridApiKey = async () => {
  await sgMail.setApiKey(process.env.SENDGRID_API_KEY)
}

const sendBadRequestError = (ctx, message = '') => {
  ctx.throw(400, `Bad request ${message}`)
}

const extractContactMailParameters = ctx => {
  let message = '<strong>Contact Request</strong>'
  let fullName = 'Anonymous'
  let email = 'gil.fernandes@bkconnect.net'
  let securityContactCode = ''
  let subject = 'Contact request from'
  let language = 'en'
  if (ctx.request.body) {
    const { body } = ctx.request
    console.log('mail body', body)
    const parsedBody = JSON.parse(body.data)
    message = parsedBody.message
    fullName = parsedBody.fullName
    language = parsedBody.language
    email = parsedBody.email
    subject = getLangToken(language, subject)
    subject = `${subject} ${fullName}`
    securityContactCode = parsedBody.securityContactCode
  } else {
    sendBadRequestError("Mail requres missing body data.")
  }
  return { subject, fullName, email, message, securityContactCode, language }
}

function sendForbiddenError (ctx) {
  err = { status: 403 }
  ctx.body = 'Forbidden'
  ctx.app.emit('error', err, ctx)
}

async function sendMailSafely (msg, ctx) {
  try {
    await sgMail.send(msg)
    ctx.send({ message: 'Mail sent successfully! ' })
    return true
  } catch (e) {
    strapi.log.error('Email could not be sent!', e)
    return false
  }
}

async function sendContactEmail (contactMail, ctx) {
  const {message, fullName, email, subject, language} = contactMail
  const contactEmails = chooseTargetEmails(language)
  console.log('Sending emails to ', contactEmails)
  const mailResult = []
  for (const targetEmail of contactEmails) {
    const origMessageSnippet = getLangToken(language, 'Original message from')
    const enhancedMessage = `${message}<p>${origMessageSnippet} ${fullName} (${email})</p>`
    const msg = buildMessage(targetEmail, subject, enhancedMessage)
    msg.replyTo = email
    strapi.log.info('final msg', msg)
    const success = await sendMailSafely(msg, ctx)
    mailResult.push(success)
  }
  return mailResult
}

module.exports = {
  // GET /mail
  index: async ctx => {
    strapi.log.info('mail parameters', ctx.query)
    ctx.send({ message: 'Mail sender docs!' })
  },
  processMultipartContent: processMultipartContent,
  extractEmail: extractEmail,
  extractTitle: extractTitle,
  // POST /mail
  send: async ctx => {
    strapi.log.info('mail parameters', ctx.query)
    await setSendGridApiKey()
    let { mailContent, targetEmail, subject, securityCode } = extractParameters(
      ctx)
    if (securityCodeOk(securityCode)) {
      const msg = buildMessage(targetEmail, subject, mailContent)
      await sendMailSafely(msg, ctx)
    } else {
      sendForbiddenError(ctx)
    }
  },
  contact: async ctx => {
    strapi.log.info('contact mail parameters', ctx.request.body)
    await setSendGridApiKey()
    let contactMail = extractContactMailParameters(ctx)
    let { message, securityContactCode } = contactMail
    strapi.log.info('contactMail', contactMail)
    if (securityContactCodeOk(securityContactCode) && message.length < contactMessageLimit) {
      const mailResult = await sendContactEmail(contactMail, ctx)
      ctx.send({ message: mailResult.every((e) => e) ? 'Success' : 'Fail' })
    } else {
      sendForbiddenError(ctx)
    }
  },
}
