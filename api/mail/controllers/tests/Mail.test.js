
const mail = require('../../../mail/controllers/Mail');

const input = {
  "what is the meaning of life?": "To be creative",
  "How is the weather today?": "The weather is kind of gray.",
  "email": "gil.fernandes@gmail.com",
  "title": "This is a test"
};

function sum(a, b) {
  return a + b;
}

// test('adds 1 + 2 to equal 3', () => {
//   expect(sum(1, 2)).toBe(3);
// });
//
// test('reads simple structure and provides HTML', () => {
//   const input = {'what is the meaning of life?': 'To be creative'};
//   const html = mail.processMultipartContent(input);
//   expect(html).toBe('<dl><dt>what is the meaning of life?</dt><dd>To be creative</dd></dl>');
// });
//
// test('reads structure with multiple elements and provides HTML', () => {
//   const input = {'what is the meaning of life?': 'To be creative', 'How is the weather today?': 'The weather is kind of gray.'};
//   const html = mail.processMultipartContent(input).replace(/\sstyle=".+?"/g, "");
//   console.log(html);
//   expect(html).toBe('<dl><dt>what is the meaning of life?</dt><dd>To be creative</dd><dt>How is the weather today?</dt><dd>The weather is kind of gray.</dd></dl>');
// });
//
// test('reads structure with multiple elements and email and provides HTML', () => {
//   const input = {
//     'what is the meaning of life?': 'To be creative',
//     'How is the weather today?': 'The weather is kind of gray.',
//     'email': 'gil.fernandes@gmail.com'
//   };
//   const html = mail.processMultipartContent(input).replace(/\sstyle=".+?"/g, "");
//   expect(html).toBe('<dl><dt>what is the meaning of life?</dt><dd>To be creative</dd><dt>How is the weather today?</dt><dd>The weather is kind of gray.</dd></dl>');
// });
//
// test('reads structure with multiple elements, email and title and provides HTML', () => {
//   const input = {
//     'what is the meaning of life?': 'To be creative',
//     'How is the weather today?': 'The weather is kind of gray.',
//     'email': 'gil.fernandes@gmail.com',
//     'title': 'This is a test'
//   };
//   const html = mail.processMultipartContent(input);
//   expect(html).toBe('<dl><dt>what is the meaning of life?</dt><dd>To be creative</dd><dt>How is the weather today?</dt><dd>The weather is kind of gray.</dd></dl>');
// });
//
// test('reads structure with email and returns email', () => {
//   const email = mail.extractEmail(input);
//   expect(email).toBe('gil.fernandes@gmail.com');
// });
//
// test('reads structure with title and returns email', () => {
//   const title = mail.extractTitle(input);
//   expect(title).toBe('This is a test');
// });
