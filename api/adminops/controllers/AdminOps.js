const execSync = require('child_process').execSync;

module.exports = {
  // GET /ops/restartlm
  // This method restarts the client of learn meditation online, so that the pages get re-compiled
  restartlm: async ctx => {
    strapi.log.info('mail parameters', ctx.query)
    const output = execSync('sudo /bin/systemctl restart learnmeditation-frontend', { encoding: 'utf-8' });  // the default is 'buffer'
    strapi.log.info('Output was:\n', output);
    ctx.send({ message: 'Learn Meditation Client restarted!' })
  }
}
