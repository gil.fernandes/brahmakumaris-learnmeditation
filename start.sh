#!/bin/bash


source /home/learnmeditation/.bashrc

cd /opt/brahmakumaris-learnmeditation
source ./sendgrid.env
yarn develop >> /tmp/brahmakumaris-learnmeditation.log 2>&1
